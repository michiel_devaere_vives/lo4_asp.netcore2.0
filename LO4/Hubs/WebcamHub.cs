﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LO4.Hubs
{
    public class WebcamHub : Hub
    {
        public async Task SendMessage(string name, string password)
        {
            await Clients.All.SendAsync("ReceiveMessage", name, password);
        }
    }
}
