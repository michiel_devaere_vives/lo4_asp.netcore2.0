﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LO4.Models;
using LO4.Services;

namespace LO4.Controllers
{
    public class HomeController : Controller
    {
        public bool error = false;
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult CreateUser()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateUser(User pUser)
        {
            if (pUser.name != null && pUser.password != null)
            {
                var mongoDbService = new UserService("myDB", "user", "mongodb://localhost");

                 await mongoDbService.Add(pUser);
            }
            else
            {
                ModelState.AddModelError("Error", "Alle velden moeten worden ingevuld");
            }

            return RedirectToAction("CreateUser");
        }

        [HttpGet]
        public IActionResult Webcam()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Webcam(User pUser)
        {
            var mongoDbService = new  UserService("myDB", "user", "mongodb://localhost");
            var users = await mongoDbService.GetUsers();

            for (int i = 0; i < users.Count ; i++)
            {
                if ((users[i].name.ToString() == pUser.name) && (users[i].password.ToString() == pUser.password))
                {
                    error = false;
                    TempData["data"] = "valid";
                    return View();
                }
                else
                {
                    error = true;
                }
                
            }

            if (error == true)
            {
                ModelState.AddModelError("Error", "Vergeet u niet eerst te registeren");
            }

            return RedirectToAction("Webcam");

        }
    }
}
