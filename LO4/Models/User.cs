﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LO4.Models
{
    public class User
    {
        [BsonId]
        public ObjectId Id { get; set; }
        [BsonRequired]
        public string name { get; set; }
        public string password { get; set; }
    }
}

