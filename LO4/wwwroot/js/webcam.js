﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/webcamHub").build();

connection.on("ReceiveMessage", function (name, password) {
    var encodedMsg = name + " is watching you";
    var li = document.createElement("li");
    li.textContent = encodedMsg;
    document.getElementById("messagesList").appendChild(li);
});

connection.start().catch(function (err) {
    return console.error(err.toString());
});

window.addEventListener("load", function (event) {
    var name = document.getElementById("userInput").value;
    var passowrd = "" ;
    connection.invoke("SendMessage", name, passowrd).catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
});
