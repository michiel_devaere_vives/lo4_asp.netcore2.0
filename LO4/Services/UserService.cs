﻿using LO4.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LO4.Services
{
    public class UserService
    {
        private IMongoCollection<User> users { get; }
        public UserService(string databaseName, string collectionName, string databaseUrl)
        {
            var mongoClient = new MongoClient(databaseUrl);
            var mongoDatabase = mongoClient.GetDatabase(databaseName);

            users = mongoDatabase.GetCollection<User>(collectionName);
        }

        public async Task Add(User pUser) => await users.InsertOneAsync(pUser);

        public async Task<List<User>> GetUsers()
        {
            var users = new List<User>();
            var bSonDocuments = await this.users.FindAsync(new BsonDocument());
            await bSonDocuments.ForEachAsync(BsonDoc => users.Add(BsonDoc));

            return users;
        }
    }
}
